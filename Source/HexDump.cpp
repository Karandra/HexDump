#include "pch.hpp"
#include "HexDump.h"
#include "StringStream.h"

namespace
{
	constexpr size_t g_AddressStringLength = sizeof(void*) * 2;

	constexpr bool IsHex(char c) noexcept
	{
		return (c >= 'a' && c <= 'f') || (c >= '0' && c <= '9');
	};
	constexpr bool IsASCII(char c) noexcept
	{
		return c >= 32 && c <= 126;
	};
}

namespace HexDump
{
	std::string DisplayHex(const std::vector<char> sourceBytes)
	{
		std::string result;
		for (size_t i = 0; i < sourceBytes.size(); i += HexRow::RowLength)
		{
			size_t rowLength = std::clamp<size_t>(sourceBytes.size() - i, 0, HexRow::RowLength);
			HexRow row(sourceBytes.data() + i, rowLength);

			result += row.ToDisplay();
			if (rowLength == HexRow::RowLength)
			{
				result += '\n';
			}
		}

		return result;
	}
	std::vector<char> DecodeHexDisplay(const std::string& hexDisplay)
	{
		std::vector<char> result;

		enum class State
		{
			RowNumber,
			RowNumberColon,
			HexContent,
			ASCII
		};

		State state = State::RowNumber;
		InputStringStream stream(hexDisplay);

		while (!stream.IsEndOfStream())
		{
			auto c = *stream.Peek();
			auto cNext = stream.PeekNext();

			switch (state)
			{
				case State::RowNumber:
				{
					// We don't have to store the parsed address, skip it
					if (!IsHex(c))
					{
						throw std::runtime_error("DecodeHexDisplay: not a hex character");
					}
					if (cNext == ':')
					{
						state = State::RowNumberColon;
					}

					// Move next
					stream.Skip(1);
					break;
				}
				case State::RowNumberColon:
				{
					// We don't need the colon either, just skip it
					if (c == ':' && cNext == ' ')
					{
						stream.Skip(2);
						state = State::HexContent;

						break;
					}
					throw std::runtime_error("DecodeHexDisplay: colon and space characters expected");
				}
				case State::HexContent:
				{
					const size_t requiredlength = HexRow::RowLength * 3 - 1;
					if (stream.GetPosition() + requiredlength > stream.GetTotalSize())
					{
						throw std::runtime_error("DecodeHexDisplay: premature end of string occurred");
					}

					// Parse row and add it to the result
					HexRow row;
					row.ParseDisplay(stream.Read(requiredlength));
					row.AppendTo(result);

					// Skip the entire hex content, two spaces and move to the next state
					stream.Skip(2);
					state = State::ASCII;

					break;
				}
				case State::ASCII:
				{
					if (IsASCII(c))
					{
						if (cNext == '\n')
						{
							// Skip new line and reset the parser
							stream.Skip(1);
							state = State::RowNumber;
						}

						// Move next
						stream.Skip(1);
						break;
					}

					throw std::runtime_error("DecodeHexDisplay: not an ASCII character");
				}
			};
		}

		return result;
	}
}

namespace HexDump
{
	void HexRow::PrintAddress(std::string& result) const
	{
		char addressBuffer[g_AddressStringLength + 1] = {};
		int length = sprintf_s(addressBuffer, "%zx", static_cast<size_t>(m_Address));

		result.append(g_AddressStringLength - length, '0');
		result.append(addressBuffer);
	}
	void HexRow::PrintHex(std::string& result) const
	{
		for (size_t i = 0; i < m_Data.size(); i++)
		{
			if (i < m_Length)
			{
				char hex[4] = {};
				sprintf_s(hex, "%02x", static_cast<unsigned int>(m_Data[i]));

				result += hex;
			}
			else
			{
				result.append(2, ' ');
			}

			if (i + 1 != m_Data.size())
			{
				result += ' ';
			}
		}
	}
	void HexRow::PrintASCII(std::string& result) const
	{
		for (size_t i = 0; i < m_Length; i++)
		{
			auto c = m_Data[i];
			if (IsASCII(c))
			{
				result += c;
			}
			else
			{
				result += '.';
			}
		}
	}

	HexRow::HexRow(const char* source, size_t length)
		:m_Length(length), m_Address(reinterpret_cast<uintptr_t>(source))
	{
		std::memcpy(m_Data.data(), source, std::min(m_Data.size(), length));
	}

	std::string HexRow::ToDisplay() const
	{
		std::string result;
		result.reserve((m_Data.size() * 4) + 12);

		PrintAddress(result);
		result.append(": ");

		PrintHex(result);
		result.append(2, ' ');

		PrintASCII(result);

		return result;
	}
	void HexRow::ParseDisplay(const std::string& row)
	{
		m_Data.fill(0);
		m_Address = 0;
		m_Length = 0;

		if (row.empty())
		{
			return;
		}

		size_t startIndex = 0;
		if (row.length() > g_AddressStringLength + 1 && row[g_AddressStringLength] == ':' && row[g_AddressStringLength + 1] == ' ')
		{
			size_t address = 0;
			sscanf_s(row.data(), "%zx", &address);
			m_Address = static_cast<uintptr_t>(address);

			// Skip to the first hex pair
			startIndex = g_AddressStringLength + 2;
		}

		size_t hexIndex = 0;
		for (size_t i = startIndex; i < row.length();)
		{
			if (i + 1 < row.length())
			{
				auto c0 = row[i];
				auto c1 = row[i + 1];

				if (IsHex(c0) && IsHex(c1))
				{
					char buffer[] = {c0, c1, 0};

					int hex = 0;
					if (sscanf_s(buffer, "%x", &hex) == 1)
					{
						m_Data[hexIndex] = static_cast<uint8_t>(hex);
						hexIndex++;

						if (hexIndex == HexRow::RowLength)
						{
							break;
						}

						// Advance to the next pair (skipping the space)
						i += 3;
						continue;
					}
				}
			}
			break;
		}

		if (hexIndex == 0)
		{
			throw std::runtime_error("HexRow::ParseDisplay: failed to parse hex-string row");
		}
		m_Length = hexIndex;
	}
}
