#include "pch.hpp"
#include "HexDump.h"
#include <iostream>
#include <iomanip>

namespace
{
	template<class T>
	bool IsBinaryEqual(const std::vector<T>& left, const std::vector<T>& right) noexcept
	{
		return left.size() == right.size() && std::memcmp(left.data(), right.data(), left.size() * sizeof(T)) == 0;
	}

	void StandardTest()
	{
		constexpr char example[] =
		{
			0x1D, 0xC4, 0x15, 0x25, 0x91, 0xE6, 0x09, 0x59, 0x04, 0x99, 0x15, 0x29,
			0x0A, 0x45, 0x21, 0x29, 0x26, 0x8E, 0x74, 0xA0, 0x1A, 0xBE, 0x75, 0x68,
			0x06, 0xDD, 0x70, 0x33, 0xA4, 0x77, 0x7A, 0x5D, 0xB1, 0xBA, 0x22, 0xA7,
			0xCF, 0xCC, 0xF7, 0xEF, 0xB1, 0xE3, 0x13, 0xED, 0xF1, 0x89, 0xAD, 0xAD,
			0xB8, 0x2A, 0x52, 0x32, 0x65, 0x79, 0x43, 0x99, 0x6F, 0xC8, 0xD3, 0x8E,
			0xB2, 0x5F, 0x50, 0xC9, 0x08, 0x4A, 0x12, 0x25, 0x79, 0xC2, 0xDD, 0x31,
			0x6B, 0xB8, 0x77, 0x74, 0x4B, 0x68, 0x4B, 0xD4, 0xDB, 0x4E, 0x92, 0x09,
			0xD5, 0x4C, 0x9F, 0x0B, 0xFD, 0xA9, 0xD1
		};

		const std::vector<char> source = {std::begin(example), std::end(example)};
		auto hexDisplay = HexDump::DisplayHex(source);
		auto hexDisplayDecoded = HexDump::DecodeHexDisplay(hexDisplay);

		std::cout << hexDisplay << std::endl;
		std::cout << "Standard test, is equal: " << std::boolalpha << IsBinaryEqual(source, hexDisplayDecoded) << std::endl << std::endl;
		std::cout << std::noboolalpha;
	}
}

int main()
{
	try
	{
		StandardTest();

		int option = -1;
		do
		{
			std::cout << "0: Enter binary data" << std::endl;
			std::cout << "1: Enter hex-view" << std::endl;
			std::cout << "-1: Exit" << std::endl;

			std::cin >> option;
			std::cin.ignore();

			switch (option)
			{
				case 0:
				{
					std::cout << "Enter a string to be interpreted as binary data to display the hex-view of it" << std::endl;

					std::string binaryData;
					std::getline(std::cin, binaryData);

					if (!binaryData.empty())
					{
						auto hexDisplay = HexDump::DisplayHex({binaryData.begin(), binaryData.end()});
						std::cout << hexDisplay << std::endl << std::endl;
					}
					else
					{
						std::cout << "Empty input" << std::endl << std::endl;
					}
					break;
				}
				case 1:
				{
					std::cout << "Enter a hex-view to extract byte data from it" << std::endl;
					std::cout << "Line-by-line input, commit with an empty line" << std::endl;

					// Read the hex-view line-by-line
					std::string hexView;
					while (!std::cin.eof())
					{
						std::string line;
						std::getline(std::cin, line);

						if (!line.empty())
						{
							if (!hexView.empty())
							{
								hexView += '\n';
							}
							hexView += line;
						}
						else
						{
							break;
						}
					}

					if (!hexView.empty())
					{
						auto bytes = HexDump::DecodeHexDisplay(hexView);
						std::cout << "Decoded " << bytes.size() << " bytes, reconstructing hex-view:" << std::endl;

						hexView = HexDump::DisplayHex(bytes);
						std::cout << hexView << std::endl << std::endl;
					}
					else
					{
						std::cout << "Empty input" << std::endl << std::endl;
					}
					break;
				}
			};
		}
		while (option != -1);
	}
	catch (const std::exception& e)
	{
		std::cout << "std::exception: " << e.what() << std::endl;
		return -1;
	}
	return 0;
}
