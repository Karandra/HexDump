#pragma once
#include "Common.h"
#include <optional>

namespace HexDump
{
	enum class StreamSeek
	{
		FromCurrent,
		FromStart,
		FromEnd
	};
}

namespace HexDump
{
	class InputStringStream final
	{
		private:
			const std::string& m_String;
			size_t m_Position = 0;

		public:
			InputStringStream(const std::string& source) noexcept
				:m_String(source)
			{
			}

		public:
			bool IsEndOfStream() const noexcept
			{
				return m_Position >= m_String.size();
			}

			size_t GetPosition() const noexcept
			{
				return m_Position;
			}
			size_t GetTotalSize() const noexcept
			{
				return m_String.size();
			}
			size_t GetDataLeft() const noexcept
			{
				return m_String.size() - m_Position;
			}
			size_t Seek(StreamSeek mode, intptr_t offset) noexcept;
			void Rewind() noexcept
			{
				Seek(StreamSeek::FromStart, 0);
			}
			void Skip(size_t offset) noexcept
			{
				Seek(StreamSeek::FromCurrent, offset);
			}

			size_t Read(char* buffer, size_t size) noexcept;
			std::string Read(size_t size)
			{
				std::string result;
				result.resize(size);
				result.resize(Read(result.data(), size));

				return result;
			}
			std::optional<char> Peek() const noexcept;
			std::optional<char> PeekNext() const noexcept;
	};
}
