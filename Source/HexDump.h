#pragma once
#include "Common.h"

namespace HexDump
{
	std::string DisplayHex(const std::vector<char> sourceBytes);
	std::vector<char> DecodeHexDisplay(const std::string& hexDisplay);
}

namespace HexDump
{
	class HexRow final
	{
		public:
			static inline constexpr size_t RowLength = 16;

		private:
			std::array<uint8_t, RowLength> m_Data = {};
			size_t m_Length = 0;
			uintptr_t m_Address = 0;

		private:
			void PrintAddress(std::string& result) const;
			void PrintHex(std::string& result) const;
			void PrintASCII(std::string& result) const;

		public:
			HexRow() = default;
			HexRow(const char* source, size_t length);

		public:
			const auto& GetContent() const
			{
				return m_Data;
			}
			uintptr_t GetAddress() const
			{
				return m_Address;
			}
			size_t GetLength() const
			{
				return m_Length;
			}

			std::string ToDisplay() const;
			void ParseDisplay(const std::string& row);

			template<class T>
			void AppendTo(std::vector<T>& result) const
			{
				result.reserve(result.size() + m_Length);
				for (size_t i = 0; i < m_Length; i++)
				{
					result.emplace_back(static_cast<T>(m_Data[i]));
				}
			}
	};
}
