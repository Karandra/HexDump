#pragma once

#include <cstdio>
#include <cstdint>

#include <memory>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <utility>
#include <type_traits>
#include <exception>
#include <stdexcept>
