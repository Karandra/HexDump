#include "pch.hpp"
#include "StringStream.h"

namespace HexDump
{
	size_t InputStringStream::Seek(StreamSeek mode, intptr_t offset) noexcept
	{
		if (m_String.empty())
		{
			m_Position = 0;
			return 0;
		}

		switch (mode)
		{
			case StreamSeek::FromCurrent:
			{
				m_Position += offset;
				break;
			}
			case StreamSeek::FromStart:
			{
				if (offset >= 0)
				{
					m_Position = offset;
				}
				break;
			}
			case StreamSeek::FromEnd:
			{
				if (offset >= 0 && offset < m_String.size())
				{
					m_Position = m_String.size() - offset - 1;
				}
				break;
			}
		};

		m_Position = std::clamp<size_t>(m_Position, 0, m_String.size());
		return m_Position;
	}

	size_t InputStringStream::Read(char* buffer, size_t size) noexcept
	{
		if (!IsEndOfStream())
		{
			const auto read = std::clamp<size_t>(size, 0, GetDataLeft());

			std::memcpy(buffer, m_String.data() + m_Position, read);
			m_Position += read;

			return read;
		}
		return 0;
	}
	std::optional<char> InputStringStream::Peek() const noexcept
	{
		if (!IsEndOfStream())
		{
			return m_String[m_Position];
		}
		return {};
	}
	std::optional<char> InputStringStream::PeekNext() const noexcept
	{
		size_t next = m_Position + 1;
		if (next < m_String.size())
		{
			return m_String[next];
		}
		return {};
	}
}
